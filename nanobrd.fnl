;; title:   nanobrd
;; author:  parlortricks
;; desc:    fennel conversion of daves84 nanobrd
;; site:    https://daves84.itch.io/nanobrd
;; license: MIT
;; SPDX-License-Identifier: MIT
;; version: 1.0
;; script:  fennel
;; strict:  true
;; input: gamepad
;; saveid: nanobrd-1

(var (a t z p w s) (values 60 0 0 0 240 math.sin))
(var y a)

(fn _G.TIC []
  (cls)
  (when (btn 4)
    (set z 3)
    (sfx 0 0))
  (when (> z (- 3))
    (set z (- z 0.5)))
  (set y (- y z))
  (for [i 0 4 1]
    (rect (% (+ (- w (/ t 2)) (* i a)) w) 0 10 136 2)
    (rect (% (+ (- w (/ t 2)) (* i a)) w) (+ 40 (* 30 (s i))) 10 30 0))
  (when (or (or (= (pix 20 y) 2) (> y 136)) (< y 0))
    (set y a)
    (set t 0))
  (print (.. "nanobrd " (// t 120)) 0 0 12)
  (for [x 0 w]
    (for [y 0 136]
      (pix x y (+ (+ (+ (pix x y) (s (/ (+ x t) a))) (s (/ y a)))
                  (s (bxor x y))))))
  (circ 20 y 2 12)
  (sfx 0 (math.floor (/ y 3)))
  (set t (+ t 1)))

